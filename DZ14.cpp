﻿#include <iostream>
#include <stdint.h>
#include <string>

int main()
{
	std::cout << "Enter your first name: ";
	std::string firstname;
	std::getline (std::cin, firstname);

	std::cout << "Enter your last name: ";
	std::string lastname;
	std::getline(std::cin, lastname);

	int a = firstname.length();
	int b = lastname.length();
	
	std::cout << "Your full name: " << firstname << " " << lastname << "\n" << '\n';
	std::cout << "Your full name has " << a + b << " letters" << "\n";
	std::cout << "First letter of your full name: " << firstname[0] << '\n';
	std::cout << "Last letter of your full name: " << lastname[b - 1] << '\n' << '\n';

	std::cout << "Your first name has " << a << " letters" << "\n";
	std::cout << "First letter of your first name: " << firstname[0] << '\n';
	std::cout << "Last letter of your first name: " << firstname[a - 1] << '\n' << '\n';

	std::cout << "Your last name has " << b << " letters" << "\n";
	std::cout << "First letter of your last name: " << lastname[0] << '\n';
	std::cout << "Last letter of your last name: " << lastname[b - 1] << '\n';
}